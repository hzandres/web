import React, { Component } from "react";
import Navbar from "./Components/Navbar";
import CardDeck from "./Components/CardDeck";
import SideCard from "./Components/SideCard";

class App extends Component {
  render() {
    return (
      <div style={{ paddingTop: "100px" }}>
        <div className="container-fluid">
          <Navbar />
          <div className="row">
            <div className="col">
              <SideCard />
            </div>
            <div className="col-8">
              <CardDeck />
            </div>
            <div className="col" />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
