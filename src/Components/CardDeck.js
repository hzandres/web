import React, {useState} from "react";
import MapCard from "./MapCards";
import MapBox from "./MapBox";
import GoogleMap from "./GoogleMap";

const cards = [
  {
    title: "Daily Reports",
    description: "Disntance: 20mi | Average Speed: 30mph",
    map: <MapBox />,
    type: "report"
  },
  {
    title: "Alert | Geofence Exception",
    description: "Violation of Geofence1",
    map: <GoogleMap />,
    type: alert
  }
];

function CardDeck() {
  const layers = useState([])
  return (
    <div>
      {cards.map((card, key) => (
        <div key={key}>
        <MapBox  layers={layers} />
          <div className="p-4" />
        </div>
      ))}
    </div>
  );
}

export default CardDeck;
