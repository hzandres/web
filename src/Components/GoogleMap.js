import React, { useRef, useLayoutEffect } from "react";
import rawLineString from "../Data/geojson-spytect-lineString";
import correctLineString from "../Data/google-corrected";

const mapStyle = {
  height: "400px",
  width: "100%"
};

function GoogleMap(props) {
  const map = useRef(null);
  useLayoutEffect(() => {
    const goo = new window.google.maps.Map(map.current, {
      center: { lat: 47.608013, lng: -122.335167 },
      zoom: 10,
      disableDefaultUI: true
    });

    const rawData = new window.google.maps.Data({ map: goo });

    rawData.addGeoJson(rawLineString);

    rawData.setStyle({
      strokeColor: "#ccc",
      strokeOpacity: 0.8,
      strokeWeight: 4
    });

    const correctedData = new window.google.maps.Data({ map: goo });

    correctedData.addGeoJson(correctLineString);

    correctedData.setStyle({
      strokeColor: "#6F3D96",
      strokeOpacity: 1,
      strokeWeight: 3
    });
  });

  return (
    <div
      ref={map}
      className="bg-danger border-bottom border-top"
      style={mapStyle}
    />
  );
}

export default GoogleMap;
