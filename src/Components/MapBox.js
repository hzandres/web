import mapboxgl from "mapbox-gl";
import React, { useRef, useLayoutEffect } from "react";
import rawLineString from "../Data/geojson-spytect-lineString";
import correctLineString from "../Data/geojson-corrected";

mapboxgl.accessToken =
  "pk.eyJ1Ijoic3B5dGVjIiwiYSI6ImNqc21uanh5YTAzaTA0OW1sZHhtNXRsNnEifQ.5B5Utoosf5kebAeRV6gSuA";

const mapStyle = {
  height: "400px",
  width: "100%"
};

function MapBox({layers}) {
  const map = useRef(null);

  useLayoutEffect(() => {
    const box = new mapboxgl.Map({
      container: map.current,
      style: "mapbox://styles/mapbox/streets-v11",
      center: [-122.335167, 47.608013],
      zoom: 10
    });

    box.on("load", () => {
      box.addLayer({
        id: "rawLineString",
        type: "line",
        source: { type: "geojson", data: layers },
        layout: {
          "line-join": "round",
          "line-cap": "round"
        },
        paint: {
          "line-color": "#ccc",
          "line-width": 4
        }
      });

      box.addLayer({
        id: "correctedlineString",
        type: "line",
        source: { type: "geojson", data: correctLineString },
        layout: {
          "line-join": "round",
          "line-cap": "round"
        },
        paint: {
          "line-color": "#6F3D96",
          "line-width": 3
        },
        filter: ["==", "$type", "LineString"]
      });
    });

    return () => {
      box.off("load");
    };
  });

  return (
    <div
      ref={map}
      className="bg-danger border-bottom border-top"
      style={mapStyle}
    />
  );
}

export default MapBox;
