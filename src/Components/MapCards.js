import React from "react";

function MapCard(props) {
  return (
    <div className="card shadow m-b-5">
      <div className="card-body">
        <h5 className="card-title">{props.card.title}</h5>
        <p className="card-text">{props.card.description}</p>
      </div>
      {props.card.map}
      <div className="card-body">
        <p className="card-text">
          <small className="text-muted">Last updated 3 mins ago</small>
        </p>
      </div>
    </div>
  );
}

export default MapCard;
