/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";

const brandStyle = {
  letterSpacing: "2px"
};

function Navbar() {
  return (
    <header className="navbar navbar-expand-lg navbar-dark fixed-top bg-dark p-3 m-l-3 shadow">
      <a className="navbar-brand m-r-8" href="#" style={brandStyle}>
        Spytec
      </a>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNavAltMarkup"
        aria-controls="navbarNavAltMarkup"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon" />
      </button>
      <div className="navbar-nav">
        <a className="nav-item nav-link active" href="#">
          Home <span className="sr-only">(current)</span>
        </a>
        <a className="nav-item nav-link" href="#">
          Features
        </a>
        <a className="nav-item nav-link" href="#">
          Pricing
        </a>
        <a
          className="nav-item nav-link disabled"
          href="#"
          tabIndex="-1"
          aria-disabled="true"
        >
          Disabled
        </a>
      </div>
    </header>
  );
}

export default Navbar;
