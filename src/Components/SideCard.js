import React, { Component } from "react";

class SideCard extends Component {
  render() {
    return (
      <div className="sticky-top">
        <div className="card shadow">
          <img src="" className="card-img-top" alt="" />
          <div className="card-body">
            <h5 className="card-title">John Snow</h5>
            <p className="card-text">
              Three active deices, last reported a 1 minute ago
            </p>
          </div>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">Device 1</li>
            <li className="list-group-item">Device 2</li>
            <li className="list-group-item">Device 3</li>
          </ul>
        </div>
      </div>
    );
  }
}

export default SideCard;
