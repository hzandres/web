const fs = require("fs");
const request = require("request");
const geojsonTidy = require("@mapbox/geojson-tidy");
const moment = require("moment");

const rawSpytectPoints = JSON.parse(
  fs.readFileSync("../data/raw-spytect-points.json", "utf-8")
);

const pointFeatures = rawSpytectPoints.map(point => {
  return {
    type: "Feature",
    geometry: {
      type: "Point",
      coordinates: [point.lng, point.lat]
    },
    properties: {
      speed: point.speed,
      date: point.date,
      time: point.time,
      milage: point.milage,
      direction: point.direction
    }
  };
});

const polyLineFeature = {
  type: "Feature",
  geometry: {
    type: "LineString",
    coordinates: rawSpytectPoints.map(point => {
      return [parseFloat(point.lng), parseFloat(point.lat)];
    })
  },
  properties: {
    coordTimes: rawSpytectPoints.map(point => {
      let date = new Date(`${point.date} ${point.time}`);
      let formatted = moment(date).toISOString();
      return formatted;
    })
  }
};

const featureCollection = {
  type: "FeatureCollection",
  features: [polyLineFeature]
};

fs.writeFileSync(
  "../data/geojson-spytect-points.json",
  JSON.stringify(featureCollection)
);

const tidyLineString = geojsonTidy.tidy(featureCollection, {
  minimumDistance: 50, // Minimum distance between points in metres
  minimumTime: 5, // Minimum time interval between points in seconds
  maximumPoints: 100 // Maximum points in a feature
});

fs.writeFileSync(
  "../data/geojson-spytect-points-tidy.json",
  JSON.stringify(tidyLineString)
);

function getPoints(index) {
  const points = tidyLineString.features[index].geometry.coordinates.reduce(
    (acc, curr) => `${acc}${curr[0]},${curr[1]};`,
    ""
  );
  return points.substring(0, points.length - 1);
}
function getTime(index) {
  const time = tidyLineString.features[index].properties.coordTimes.reduce(
    (acc, curr) => `${acc}${moment(curr).valueOf()};`,
    ""
  );
  return time.substring(0, time.length - 1);
}
function getParams(index) {
  const url = "https://api.mapbox.com/matching/v5/mapbox/driving/";
  return {
    method: "GET",
    url: `${url}${getPoints(index)}`,
    qs: {
      access_token:
        "pk.eyJ1Ijoic3B5dGVjIiwiYSI6ImNqc21uanh5YTAzaTA0OW1sZHhtNXRsNnEifQ.5B5Utoosf5kebAeRV6gSuA",
      geometries: "geojson",
      timestamps: `${getTime(index)}`
    },
    json: true,
    headers: {
      "Postman-Token": "b87c717c-8a19-4fe2-9845-ec444e860894",
      "cache-control": "no-cache"
    }
  };
}

function makeRequest(index) {
  request(getParams(index), (err, response, body) => {
    if (err) return err;
    fs.writeFileSync(
      `../data/mapbox-corrected-${index}.json`,
      JSON.stringify(body)
    );

    if (index + 1 < tidyLineString.features.length) {
      makeRequest(index + 1);
    }
  });
}

makeRequest(0);

const f1 = JSON.parse(
  fs.readFileSync("../data/mapbox-corrected-0.json", "utf-8")
).matchings.map(t => {
  return { type: "Feature", geometry: t.geometry };
});

const f2 = JSON.parse(
  fs.readFileSync("../data/mapbox-corrected-1.json", "utf-8")
).matchings.map(t => {
  return { type: "Feature", geometry: t.geometry };
});

const correctedLineString = {
  type: "FeatureCollection",
  features: f1.concat(f2)
};

fs.writeFileSync(
  `../data/geojson-corrected.js`,
  JSON.stringify(correctedLineString)
);
