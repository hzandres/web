const fs = require("fs");
const request = require("request");
const geojsonTidy = require("@mapbox/geojson-tidy");
const moment = require("moment");

const featureCollection = JSON.parse(
  fs.readFileSync("../data/geojson-spytect-points.json", "utf-8")
);

const tidyLineString = geojsonTidy.tidy(featureCollection, {
  minimumDistance: 5, // Minimum distance between points in metres
  minimumTime: 5, // Minimum time interval between points in seconds
  maximumPoints: 100 // Maximum points in a feature
});

function getPoints(index) {
  const points = tidyLineString.features[index].geometry.coordinates.reduce(
    (acc, curr) => `${acc}${curr[1]},${curr[0]}|`,
    ""
  );
  return points.substring(0, points.length - 1);
}
function getTime(index) {
  const time = tidyLineString.features[index].properties.coordTimes.reduce(
    (acc, curr) => `${acc}${moment(curr).valueOf()};`,
    ""
  );
  return time.substring(0, time.length - 1);
}
function getParams(index) {
  return {
    method: "GET",
    url: "https://roads.googleapis.com/v1/snapToRoads",
    qs: {
      key: "AIzaSyBC0-YkZAcV0hMAZ5-Fc8MJln66xh5G3fg",
      interpolate: "true",
      path: getPoints(index)
    },
    json: true
  };
}

function makeRequest(index) {
  request(getParams(index), (err, response, body) => {
    if (err) return err;
    fs.writeFileSync(
      `../data/google-corrected-${index}.json`,
      JSON.stringify(body)
    );

    if (index + 1 < tidyLineString.features.length) {
      makeRequest(index + 1);
    }
  });
}

//makeRequest(0);

function getCorrectedPoints(file) {
  return JSON.parse(fs.readFileSync(file, "utf-8")).snappedPoints.map(t => {
    return [t.location.longitude, t.location.latitude];
  });
}

const cp1 = getCorrectedPoints("../Data/google-corrected-0.json");
const cp2 = getCorrectedPoints("../Data/google-corrected-1.json");
const cp3 = getCorrectedPoints("../Data/google-corrected-2.json");

const correctedLineString = {
  type: "FeatureCollection",
  features: [
    {
      type: "Feature",
      geometry: {
        type: "LineString",
        coordinates: cp1.concat(cp2).concat(cp3)
      }
    }
  ]
};

fs.writeFileSync(
  `../data/google-corrected.js`,
  JSON.stringify(correctedLineString)
);
