const mbxClient = require("@mapbox/mapbox-sdk");
const mbxMapMatchingClient = require("@mapbox/mapbox-sdk/matching");

const baseClient = mbxClient({
  accessToken:
    "pk.eyJ1Ijoic3B5dGVjIiwiYSI6ImNqc21uanh5YTAzaTA0OW1sZHhtNXRsNnEifQ.5B5Utoosf5kebAeRV6gSuA"
});

const mapMatchingClient = mbxMapMatchingClient(baseClient);
